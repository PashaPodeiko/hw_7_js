
//Теоретический ответ. DOM - это способ представления дерева тегов в HTML.

/*
const showArry = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
//const showArry = ["1", "2", "3", "sea", "user", 23];

const parentElement = document.querySelector("body");

function createList(arry, parent) { // функция, которая принимает на вход массив и родитнля

	let newUl = document.createElement("ul");

	parent.prepend(newUl);

	arry.map(function (elemtnt) { // функция вывода элементов массива на страницу в виде пункта списка в теле родителя

		let newLi = document.createElement("li")

		newLi.innerText = elemtnt;

		newUl.append(newLi);

	});
}
createList(showArry, parentElement);
*/

//================== Second edition ========================================

const arry = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
//const showArry = ["1", "2", "3", "sea", "user", 23];

const parentElement = document.querySelector("body");

const content = arry.map(element => `<li>${element}</li>`).join(""); // join("") - склеиваем плученные <li> в одну строку (без ,)

const newUl = document.createElement("ul");

newUl.innerHTML = content;

parentElement.append(newUl);


